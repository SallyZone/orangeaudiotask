//
//  SearchViewModel.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/7/18.
//  Copyright (c) 2018 Sally. All rights reserved.
//

import RxSwift
import RxAlamofire
import RxOptional
class SearchViewModel {
    var tracksPublishSubject = PublishSubject<[Track]>()
    var errorSubject = PublishSubject<BaseError>()

    // input
    var nextPageLink :String?
    private var maintracks:[Track]!
    func searchfor(_ searchWord:String){
        log(message: "search tracks", type: .info, argment: [searchWord])
        let request = BaseRequestFactory(apiMethod: "search", httpMethod: .get, parameters: ["q":searchWord])
        maintracks = []
        self.getRemoteRequest(request)
    }

    func loadMore(){
        guard let nextPage = self.nextPageLink , !(nextPage.isEmpty) else {return}
        log(message: "load more tracks", type: .info, argment: [nextPage])
        let request = BaseRequestFactory(url: nextPage)
        self.getRemoteRequest(request)
    }

    fileprivate func getRemoteRequest(_ request:BaseRequestFactory){
    log(message: "get tracks remote", type: .info, argment: [request.getRequest()])
        self.nextPageLink = ""
        APIClient().executeRequest(request.getRequest(), success: { [weak self](searchResult) in
            print(searchResult)
            self?.nextPageLink = (searchResult["next"]  as? String) ?? ""

            if let data:[[String : Any]] = searchResult["data"] as? [[String : Any]]{
                if let jsonData = try? JSONSerialization.data(withJSONObject: data, options: []){
                    let tracks = try? JSONDecoder().decode([Track].self, from: jsonData )
                    if let tracks = tracks{
                        self?.maintracks.append(contentsOf: tracks)
                        TrackDataManager().saveTrackArray(tracks)
                        self?.tracksPublishSubject.onNext((self?.maintracks)!)
                    }
                }
            }
        }) { (error) in
            print(error)
            self.errorSubject.onNext(error)
        }
    }
    // output


    // internal

    init() {
        setupRx()
    }
}

// MARK: Setup
private extension SearchViewModel {

    func setupRx() {

    }


}
