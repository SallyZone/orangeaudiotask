//
//  Track.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/8/18.
//  Copyright © 2018 Sally. All rights reserved.
//

import UIKit
import RealmSwift

class Track:  Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var link: String?
    @objc dynamic var preview: String?
    @objc dynamic var rank: Int = 0
    @objc dynamic var readable: Bool = false
    @objc dynamic var title: String?
    @objc  dynamic var titleShort: String?
    @objc dynamic var titleVersion: String?
    @objc dynamic var type: String?
    @objc dynamic var coverMedium: String?
    @objc dynamic var cover: String?
    var isPlaying: Bool = false
    override class func primaryKey() -> String? {
        return "id"
    }
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case link = "link"
        case preview = "preview"
        case rank = "rank"
        case readable = "readable"
        case title = "title"
        case titleShort = "title_short"
        case titleVersion = "title_version"
        case type = "type"
        case album


    }
    enum AlbumKeys: String, CodingKey {
        case coverMedium = "cover_medium"
        case cover = "cover"
    }
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        link = try values.decodeIfPresent(String.self, forKey: .link)
        preview = try values.decodeIfPresent(String.self, forKey: .preview)
        rank = try values.decode(Int.self, forKey: .rank)
        readable = try values.decode(Bool.self, forKey: .readable)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        titleShort = try values.decodeIfPresent(String.self, forKey: .titleShort)
        titleVersion = try values.decodeIfPresent(String.self, forKey: .titleVersion)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        let albumContainer =
            try values.nestedContainer(keyedBy: AlbumKeys.self, forKey: .album)
        coverMedium = try albumContainer.decodeIfPresent(String.self, forKey: .coverMedium)
        cover = try albumContainer.decodeIfPresent(String.self, forKey: .cover)

    }
}
