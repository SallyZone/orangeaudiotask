//
//  TrackCollectionViewCell.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/7/18.
//  Copyright © 2018 Sally. All rights reserved.
//

import UIKit
import AlamofireImage
import RxSwift
import RxCocoa

class TrackCollectionViewCell: UICollectionViewCell {
    var disposeBag = DisposeBag()
    @IBOutlet weak var imagBg: UIImageView!
    @IBOutlet weak var otherMediaControl: UIView!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    var track:Track!{
        didSet{
            self.titleLbl.text = track.title
            if let cover:String = track.coverMedium,  let coverURL:URL = URL(string: cover){
                imagBg.af_setImage(withURL: coverURL)
            }
            self.playBtn.isEnabled = !track.isPlaying
            self.stopBtn.isEnabled = track.isPlaying
            
            self.playBtn.rx.tap.bind { [weak self] in
                log(message: "play audio", type: .info, argment: [])
                self?.track.isPlaying = true
                AudioPlayerManager.shared.stop()
                AudioPlayerManager.shared.play(url: URL(string: (self?.track.preview!)!)!)
                self?.playBtn.isEnabled = !(self?.track.isPlaying)!
                self?.stopBtn.isEnabled = (self?.track.isPlaying)!
                }.disposed(by: self.disposeBag)

            self.stopBtn.rx.tap.bind { [weak self]  in
                log(message: "stop audio", type: .info, argment: [])
                self?.track.isPlaying = false
                AudioPlayerManager.shared.stop()
                self?.playBtn.isEnabled = !(self?.track.isPlaying)!
                self?.stopBtn.isEnabled = (self?.track.isPlaying)!
                }.disposed(by: self.disposeBag)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.stopBtn.setTitleColor(UIColor.gray, for: UIControlState.disabled)
        self.playBtn.setTitleColor(UIColor.gray, for: UIControlState.disabled)
       self.disposeBag = DisposeBag()
    }

}
