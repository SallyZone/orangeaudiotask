//
//  SearchViewController.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/7/18.
//  Copyright (c) 2018 Sally. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxOptional

class SearchViewController: UIViewController  {
    fileprivate let viewModel: SearchViewModel
    fileprivate let router: SearchRouter
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentView: UISegmentedControl!
    var numberOfItemsPerPage = 1
    var heightOfCell:CGFloat = 130
    static let startLoadingOffset: CGFloat = 20.0

    init(withViewModel viewModel: SearchViewModel, router: SearchRouter) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupLayout()
        setupRx()
    }
}

// MARK: Setup
private extension SearchViewController {

    func setupViews() {
        let nibCell = UINib(nibName: "TrackCollectionViewCell", bundle: nil)
        collectionView.register(nibCell, forCellWithReuseIdentifier: "TrackCollectionViewCell")
    }

    func setupLayout() {
        adjustCollectionFlow()
    }

    func adjustCollectionFlow() {
        // adjust flow layout
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        let itemSpacing: CGFloat = 1
        let itemsInOneLine: CGFloat = CGFloat(self.numberOfItemsPerPage)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = collectionView.bounds.size.width - (itemSpacing * CGFloat(itemsInOneLine - 1))
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 1
        flowLayout.itemSize = CGSize(width: floor(width/itemsInOneLine), height: heightOfCell)
        self.collectionView.collectionViewLayout = flowLayout
    }

    func setupRx() {

        segmentView
            .rx
            .selectedSegmentIndex
            .bind { [weak self](index) in
            self?.numberOfItemsPerPage = index == 1 ? 1: 2
            self?.collectionView.reloadData()
        }.disposed(by: self.disposeBag)

        self.viewModel
            .errorSubject
            .bind { (error) in
            let alert = UIAlertController(title: "", message: error.message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }.disposed(by: self.disposeBag)
        
        let loadNextPageTrigger = collectionView.rx.contentOffset
            .flatMap { offset in
                SearchViewController.isNearTheBottomEdge(contentOffset: offset, self.collectionView) ? Observable.just(true) : Observable.just(false)
        }
        loadNextPageTrigger.asObservable().bind { [weak self](isLoadMore) in
            if isLoadMore {
            self?.viewModel.loadMore()
            }
        }.disposed(by: self.disposeBag)
        self.collectionView
            .rx
            .setDelegate(self)
            .disposed(by: self.disposeBag)


        self.viewModel
            .tracksPublishSubject
            .asObservable()
            .bind(to: self.collectionView.rx.items(cellIdentifier: "TrackCollectionViewCell", cellType: TrackCollectionViewCell.self)) { _, data, cell in
                cell.track = data
            }.disposed(by: self.disposeBag)

    searchBar
        .rx
        .text
        .throttle(0.5, scheduler: MainScheduler.instance)
        .filter {!($0?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty ?? false)}
        .distinctUntilChanged().bind { [weak self](searchText) in
            self?.viewModel.searchfor(searchText ?? "")
        }.disposed(by: self.disposeBag)




    }




}
extension SearchViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width  - (2 * CGFloat(numberOfItemsPerPage - 1))
        let itemWidth = floor(width/CGFloat(self.numberOfItemsPerPage))
        return CGSize(width: itemWidth, height: heightOfCell)

    }
    static func isNearTheBottomEdge(contentOffset: CGPoint, _ collectionView: UICollectionView) -> Bool {
        return contentOffset.y + collectionView.frame.size.height + SearchViewController.startLoadingOffset > collectionView.contentSize.height
    }
}
