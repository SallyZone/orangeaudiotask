//
//  SearchBuilder.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/7/18.
//  Copyright (c) 2018 Sally. All rights reserved.
//

import UIKit

struct SearchBuilder {

    static func viewController() -> UIViewController {
        let viewModel = SearchViewModel()
        let router = SearchRouter()
        let viewController = SearchViewController(withViewModel: viewModel, router: router)
        router.viewController = viewController

        return viewController
    }
}
