//
//  TrackDataManager.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/9/18.
//  Copyright © 2018 Sally. All rights reserved.
//

import UIKit
import RealmSwift

class TrackDataManager: NSObject {
    override init() {
        super.init()
       let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 2,

            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in

        })
        Realm.Configuration.defaultConfiguration = config

    }

    func saveTrackArray(_ objects: [Object]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(objects, update: true)
        }
    }


}
