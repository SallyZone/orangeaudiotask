//
//  AudioPlayerManager.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/9/18.
//  Copyright © 2018 Sally. All rights reserved.
//

import UIKit
import AVFoundation

class AudioPlayerManager: NSObject{
    static let shared: AudioPlayerManager = {
        let sharedInstance = AudioPlayerManager()
        return sharedInstance
    }()
    private override init() {
        //private init
    }
    var player : AVPlayer?
    func play(url:URL) {
        print("playing \(url)")
        log(message: "playing audio func", type: .info, argment: [url])
        log(message: "playing audio", type: .debug, argment: [url])
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            self.player = AVPlayer(url:url)
//            self.player = try AVAudioPlayer(contentsOf: url)
           // player?.prepareToPlay()
            player?.volume = 1.0
            player?.play()
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
            log(message: "error", type: .debug, argment: [error.localizedDescription])
        } catch {
            log(message: "AVAudioPlayer init failed", type: .debug, argment: [])
            print("AVAudioPlayer init failed")
        }

    }

    func stop(){
        log(message: "stop audio func", type: .info, argment: [])
        log(message: "stop audio", type: .debug, argment: [])
        self.player?.pause()
    }
}
