//
//  BaseRequestFactory.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/8/18.
//  Copyright © 2018 Sally. All rights reserved.
//

import UIKit

class BaseRequestFactory: NSObject {
    fileprivate var request: BaseRequest?
    convenience init(apiMethod: String, httpMethod: VFRequestHTTPMethod, parameters: [String: Any]? = [:], headers: [String: String]? = [:]) {
        self.init()
        request = BaseRequest()
        request?.url = baseURL.appendingFormat("/%@", apiMethod)
        request?.httpMethod = httpMethod
        request?.parameters = parameters
        request?.headers = headers

    }
    convenience init(url:String) {
        self.init()
        request = BaseRequest()
        request?.httpMethod = .get
        request?.url = url

    }

    func getRequest() -> BaseRequest {
        return request!
    }
}
