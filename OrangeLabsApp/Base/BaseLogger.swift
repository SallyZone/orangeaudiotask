//
//  BaseLogger.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/9/18.
//  Copyright © 2018 Sally. All rights reserved.
//

import UIKit
import os.log

func log(message : StaticString ,type : OSLogType,argment:[Any]?) {
    os_log(message, log: OSLog(subsystem: "", category: ""), type:type , argment ?? [])
}

