//
//  APIClient.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/8/18.
//  Copyright © 2018 Sally. All rights reserved.
//
import UIKit
import RxAlamofire
import Alamofire
import RxCocoa
import RxSwift
import Foundation
import Reachability

class APIClient: NSObject {
    override init() {
    super.init()
    }
    func executeRequest(_ baseRequest: BaseRequest, success: @escaping (_ results: [String:Any]) -> Void, failure: @escaping (_ error: BaseError) -> Void ) {
        // handle no internet connection of request
        let reachability = Reachability()
        if reachability?.connection == .none {
            // warning i need to put it in error catalog
            let noConnectionError: BaseError = BaseError(code: LocalError.notConnectedToInternet.rawValue, message: "no internet connection", api: baseRequest.apiName)
            log(message: "error", type: .error, argment: [noConnectionError])

            failure(noConnectionError)
        } else {
            let requestOb =  Alamofire.SessionManager.default.rx.request(HTTPMethod(rawValue: baseRequest.httpMethod.rawValue)!, baseRequest.url, parameters: baseRequest.parameters, encoding: (baseRequest.httpMethod == .post ?  JSONEncoding.default : URLEncoding.default), headers: baseRequest.headers).timeout(baseRequest.timeOut!, scheduler: MainScheduler.instance)
            _ = requestOb.responseJSON().subscribe(onNext: { (data) in
                if let error = data.result.error{
                    failure(BaseError(error: error))
                }else if let data : [String:Any] = data.result.value as? [String : Any] {
                    success(data)
                }
            }, onError: { (error) in
                failure(BaseError(error: error))
                log(message: "error", type: .error, argment: [BaseError(error: error)])

            })
        }
    }

}

