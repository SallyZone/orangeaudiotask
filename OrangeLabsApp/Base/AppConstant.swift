//
//  AppConstant.swift
//  OrangeLabsApp
//
//  Created by Sally on 9/8/18.
//  Copyright © 2018 Sally. All rights reserved.
//

import UIKit
let baseURL = "https://api.deezer.com"
let errorAPINameKey: String = "ErrorAPINameKey"
enum LocalError: Int {
    case dataConnectionFailed = 1
    case notConnectedToInternet
    case timedOut
    case unknownNetworkError
}


